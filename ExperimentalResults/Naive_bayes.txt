=== Classifier model (full training set) ===

Naive Bayes Classifier

                       Class
Attribute          Very poor Satisfactory         Poor     Polluted         Good
                      (0.04)       (0.22)       (0.05)       (0.62)       (0.07)
=================================================================================
CO(GT)
  mean                 4.4927       1.1067       3.6962       2.4175       0.7604
  std. dev.            1.7957       0.5065       1.6176       1.2132        0.593
  weight sum              303         1796          411         5067          553
  precision            0.1229       0.1229       0.1229       0.1229       0.1229

PT08.S1(CO)
  mean              1361.6227     942.7521    1303.6135    1157.4543     844.2916
  std. dev.          209.5977     102.6397     198.1795     190.0819      84.4881
  weight sum              303         1796          411         5067          553
  precision            1.3537       1.3537       1.3537       1.3537       1.3537

C6H6(GT)
  mean                19.0367        4.927      17.0427      11.6862       2.4999
  std. dev.            9.2235       2.6872       7.9182       6.6739       1.7558
  weight sum              303         1796          411         5067          553
  precision             0.161        0.161        0.161        0.161        0.161

PT08.S2(NMHC)
  mean              1239.2744     742.3302    1186.4431    1009.5916     602.8974
  std. dev.          255.2617     134.5483     232.9191     227.6037     109.3344
  weight sum              303         1796          411         5067          553
  precision             1.512        1.512        1.512        1.512        1.512

NOx(GT)
  mean               723.4046      93.2787     540.3953     262.6162     123.3805
  std. dev.          264.7493       61.636     219.6431     165.7967     108.8602
  weight sum              303         1796          411         5067          553
  precision            1.5968       1.5968       1.5968       1.5968       1.5968

PT08.S3(NOx)
  mean               563.9436    1009.5663     600.3317     763.1892    1228.7005
  std. dev.           139.291     227.3458     130.5273     180.8902     292.4442
  weight sum              303         1796          411         5067          553
  precision            1.9708       1.9708       1.9708       1.9708       1.9708

NO2(GT)
  mean               235.9681       65.439     192.8128     121.2901      66.5292
  std. dev.           24.9583      16.4601       7.7955      26.6665      41.3317
  weight sum              303         1796          411         5067          553
  precision            1.1943       1.1943       1.1943       1.1943       1.1943

PT08.S4(NO2)
  mean               1491.632    1355.7016     1485.854    1491.6142    1246.4421
  std. dev.          287.2942     245.3338     367.9503       368.26     249.0835
  weight sum              303         1796          411         5067          553
  precision            1.4193       1.4193       1.4193       1.4193       1.4193

PT08.S5(O3)
  mean              1618.6122     739.8642    1503.9198    1114.2711      568.616
  std. dev.          413.3973     211.4049     358.4839     334.7137       199.03
  weight sum              303         1796          411         5067          553
  precision            1.3353       1.3353       1.3353       1.3353       1.3353

T
  mean                11.7668      19.2967      14.5456      18.1582      17.8364
  std. dev.             6.831       8.2969       8.8916       8.9577       8.0533
  weight sum              303         1796          411         5067          553
  precision            0.1067       0.1067       0.1067       0.1067       0.1067

RH
  mean                 51.162      51.5919      47.3113      47.5956      52.7946
  std. dev.           16.0325      16.4829       16.691      17.6024      13.9231
  weight sum              303         1796          411         5067          553
  precision            0.1067       0.1067       0.1067       0.1067       0.1067

AH
  mean                 0.7035       1.1366       0.7863       0.9707       1.1155
  std. dev.            0.2489       0.4007       0.3418       0.3728       0.4493
  weight sum              303         1796          411         5067          553
  precision            0.0003       0.0003       0.0003       0.0003       0.0003



Time taken to build model: 0.02 seconds

=== Stratified cross-validation ===
=== Summary ===

Correctly Classified Instances        6051               74.428  %
Incorrectly Classified Instances      2079               25.572  %
Kappa statistic                          0.5854
Mean absolute error                      0.1057
Root mean squared error                  0.2893
Relative absolute error                 47.6657 %
Root relative squared error             86.9084 %
Total Number of Instances             8130     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.878    0.023    0.592      0.878    0.707      0.709    0.991     0.848     Very poor
                 0.698    0.117    0.629      0.698    0.661      0.560    0.909     0.718     Satisfactory
                 0.664    0.018    0.667      0.664    0.666      0.648    0.985     0.725     Poor
                 0.749    0.101    0.924      0.749    0.828      0.628    0.920     0.949     Polluted
                 0.839    0.094    0.395      0.839    0.537      0.534    0.947     0.641     Good
Weighted Avg.    0.744    0.097    0.798      0.744    0.758      0.611    0.925     0.862     

=== Confusion Matrix ===

    a    b    c    d    e   <-- classified as
  266    0   21   16    0 |    a = Very poor
    0 1253    0  175  368 |    b = Satisfactory
   49    0  273   88    1 |    c = Poor
  134  682  115 3795  341 |    d = Polluted
    0   58    0   31  464 |    e = Good

